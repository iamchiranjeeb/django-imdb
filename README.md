# Project : IMDB Clone REST API

## Tech Stacks Used :- 
#### i. Python3
#### ii. Django
#### iii. Docker
#### iv. MySql
#### v. Sqlite
#### vi. Redis
#### vii. AWS SQS
#### viii. AWS S3
#### ix. Shell-Script

## Procedure To Execute ->

#### -> Clone Repo

#### -> pip3 install requirements.txt

#### -> Create .env file inside imdbtest directory & insert require data same as examples.env

#### -> python3 manage.py runserver 7001

#### -> To run on Docker execute :- docker-compose up -d
