from rest_framework import permissions

class AdminOrReadOnly(permissions.IsAdminUser):
    
    def has_permission(self, request, view):
        
        adminPermisson = bool(request.user and request.user.is_staff)

        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return adminPermisson