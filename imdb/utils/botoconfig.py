
from django.conf import settings
import boto3


class BotoConfig:

    def botoConfig(self,fileName:str):

        key = "upload-file"

        s3 = boto3.resource('s3')
        bucket = s3.Bucket(settings.AWS_STORAGE_BUCKET_NAME)
        bucket.upload_file(fileName,key)
        location = boto3.client('s3', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        url = "https://s3-%s.amazonaws.com/%s/%s" % (location, settings.AWS_STORAGE_BUCKET_NAME, key)
        return url