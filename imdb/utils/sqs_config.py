from django.conf import settings
import boto3

class SQSConfig(object):

    _sqsMessage = None
    _sqsMetaData = {}
    
    def setSQSMessage(self, message:str):
        self._sqsMessage = message

    def setSQSMetaData(self,metaData:dict):
        self._sqsMetaData = metaData

    def getSQSMessage(self)->str:
        return self._sqsMessage
    
    def getSQSMetaData(self)->dict:
        return self._sqsMetaData

class SQS:

    _sqsMessage = None
    _sqsMetaData = {}
    _sqs = None

    def __init__(self):
        
        self._sqs = boto3.client('sqs', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,region_name=settings.AWS_REGION)
        
    def setSQSMessage(self, message:str):

        self._sqsMessage = message

    def setSQSMetaData(self,metaData:dict):

        self._sqsMetaData = metaData

    def sendMessage(self):
        
        self._sqs.send_message(QueueUrl=settings.SQS_URL, MessageBody=self._sqsMessage,MessageAttributes=self._sqsMetaData)