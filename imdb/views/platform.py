from rest_framework.response import Response
from rest_framework import status,viewsets
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from imdb.serializers import StreamPlatformSerializer
from imdb.models import StreamPlatform
from imdb.permissions import AdminOrReadOnly
from imdb.constants import CC
from django.core.cache import cache


class PlatformView(APIView):

    permission_classes = [IsAuthenticated]
    permission_classes = [AdminOrReadOnly]

    def get(self,request):

        try:
            
            platforms = cache.get(CC.platform)

            if platforms == None:
                platforms = StreamPlatform.objects.all()
                cache.set(CC.platform,platforms,timeout=CC.timeOut)
        
            serializer = StreamPlatformSerializer(platforms,many=True)
            return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)

        except Exception as e:

            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_400_BAD_REQUEST)

    
    def post(self, request):
        
        serializer = StreamPlatformSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({"message":serializer.data,"success":True},status=status.HTTP_201_CREATED)
        else:
            return Response({"message":serializer.errors,"success":False},status=status.HTTP_400_BAD_REQUEST)


class PlatformDetailsView(APIView):
    permission_classes = [AdminOrReadOnly]
    permission_classes = [IsAuthenticated]

    def get(self,request,pk):

        try:
            platform = StreamPlatform.objects.get(pk=pk)
        except StreamPlatform.DoesNotExist:
            return Response({"message":f"Platform Does Not Exist With Id {pk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = StreamPlatformSerializer(platform)
        return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)

    def patch(self, request,pk):
        
        try:
            platform = StreamPlatform.objects.get(pk=pk)
        except StreamPlatform.DoesNotExist:
            return Response({"message":f"Platform Does Not Exist With Id {pk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = StreamPlatformSerializer(platform,data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)
        else:
            return Response({"message":serializer.errors,"success":False},status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        
        try:
            platform = StreamPlatform.objects.get(pk=pk)
        except StreamPlatform.DoesNotExist:
            return Response({"message":f"Platform Does Not Exist With Id {pk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        deletePlatform = platform.delete()
        return Response({"message":deletePlatform,"success":True},status=status.HTTP_200_OK)