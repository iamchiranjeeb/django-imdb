from rest_framework.response import Response
from rest_framework import status,viewsets
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from imdb.serializers import MovieSerializer
from imdb.models import MoviesModel
from imdb.permissions import AdminOrReadOnly
from imdb.constants import CC
from django.core.cache import cache



class MovieView(APIView):

    permission_classes = [IsAuthenticated]
    permission_classes = [AdminOrReadOnly]

    def get(self,request):
        
        try:

            movies = cache.get(CC.movies)

            if movies == None:
                print("Caching movies..........")
                movies = MoviesModel.objects.all()
                cache.set(CC.movies, movies, timeout=CC.timeOut)
        
            serializer = MovieSerializer(movies,many=True)
            return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)

        except Exception as e:
            print(e)

            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        
        serializer = MovieSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({"data":serializer.data,"success":True},status=status.HTTP_201_CREATED)
        else:
            return Response({"error":serializer.errors,"success":False},status=status.HTTP_400_BAD_REQUEST)


class MovieDetailsView(APIView):

    permission_classes = [IsAuthenticated]
    permission_classes = [AdminOrReadOnly]

    def get(self, request, pk):

        try:
            movie = MoviesModel.objects.get(pk=pk)
        except MoviesModel.DoesNotExist:
            return Response({"message":f"Movie Does Not Exist With Id {pk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = MovieSerializer(movie)
        return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)

    
    def patch(self, request, pk):
        
        try:
            movie = MoviesModel.objects.get(pk=pk)
        except MoviesModel.DoesNotExist:
            return Response({"message":f"Movie Does Not Exist With Id {pk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = MovieSerializer(movie,data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)
        else:
            return Response({"message":serializer.errors,"success":False},status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, pk):

        try:
            movie = MoviesModel.objects.get(pk=pk)
        except MoviesModel.DoesNotExist:
            return Response({"message":f"Movie Does Not Exist With Id {pk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        deleteMovie = movie.delete()
        return Response({"message":deleteMovie,"success":True},status=status.HTTP_200_OK)