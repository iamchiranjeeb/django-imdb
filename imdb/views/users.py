from rest_framework.response import Response
from rest_framework import status,viewsets
from rest_framework.views import APIView
from imdb.serializers import UserSerializers,ProfilePhotoSerializer
from imdb.models import ProfilePhotoModel
from imdb.utils import SQS
from django.conf import settings as st
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from boto3.session import Session
from rest_framework.parsers import MultiPartParser,FileUploadParser

class UsersView(APIView):

    def __init__(self):
        self.sqs = SQS()
    
    def post(self, request):
        serializer = UserSerializers(data=request.data)
        sqsMetaData = {
            'USER_NAME':{
                'DataType':'String',
            },
            'USER_EMAIL':{
                'DataType':'String',
            }
        }


        if serializer.is_valid():
            
            data = {}
            newUser = serializer.save()

            data['response'] = "Registration Successful !!!"
            data['username'] = newUser.username
            data['email'] = newUser.email

            refresh = RefreshToken.for_user(newUser)
            refreshAccessToken = {
                'refresh': str(refresh),
                'access': str(refresh.access_token)
            }
            data['tokens'] = refreshAccessToken

            sqsMetaData['USER_NAME']['StringValue']=newUser.first_name + " " + newUser.last_name
            sqsMetaData['USER_EMAIL']['StringValue']=newUser.email
            sqsMessage = f"New User Registered: Email: {newUser.email}, Name: {newUser.username}"

            self.sqs.setSQSMessage(sqsMessage)
            self.sqs.setSQSMetaData(sqsMetaData)
            self.sqs.sendMessage()

            return Response({"message":data,"success":True},status=status.HTTP_201_CREATED)
        else:
            return Response({"message":serializer.errors,"success":False}, status=status.HTTP_400_BAD_REQUEST)


# class UserLogoutView(APIView):

    # authentication_classes = [JWTAuthentication]
    # permission_classes = [IsAuthenticated]

    # def post(self, request):
    #     # if not request.user.is_authenticated:
    #     #     print('okiiiii')

    #     try:

    #         request.user.auth_token.delete()
    #         print("Auth Token ",request.user)
    #         return Response({"message":"Logged out !!","success":True},status=status.HTTP_200_OK)
        
    #     except Exception as e:
    #         print("logout Error -->")
    #         return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_400_BAD_REQUEST)

class UserLogoutView(APIView):

    def post(self, request):

        try:

            request.user.auth_token.delete()
            print("Auth Token ",request.user)
            return Response({"message":"Logged out !!","success":True},status=status.HTTP_200_OK)

        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":True},status=status.HTTP_200_OK)


class UploadPhoto(APIView):

    permission_classes = [IsAuthenticated]
    parser_classes = (MultiPartParser,FileUploadParser)

    def get(self, request):

        try:
        
            profile_pics = ProfilePhotoModel.objects.all()
            serializer = ProfilePhotoSerializer(profile_pics,many=True)
            return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)

        except Exception as e:
            print(e)
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):

        uploaded_file = request.FILES['file2']
        # fileSerializers = ProfilePhotoSerializer.save()
        # dataSerializers = ProfilePhotoSerializer(request.d)


        # print(uploaded_file.name)
        # print(uploaded_file.content_type)
        # print(uploaded_file.content_type_extra)
        # print(uploaded_file.size)

        session = Session(region_name=st.AWS_REGION,aws_access_key_id=st.AWS_ACCESS_KEY_ID,aws_secret_access_key=st.AWS_SECRET_ACCESS_KEY)
        s3 = session.resource('s3')
        print("s3")
        print(s3)
        print("-------------")
        print(request.data)

        profilePhoto = "{}_.png".format(request.user.id)

        key = "Users/Profile/{}".format(profilePhoto)
        s3.Bucket(st.AWS_STORAGE_BUCKET_NAME).put_object(ContentType=uploaded_file.content_type,Key=key, Body=uploaded_file,ACL="public-read")

        
        
        ProfilePhotoModel.objects.update_or_create(user=request.user, photo_name=profilePhoto)
        # print(dataSerializers.user_id)


        # if dataSerializers.is_valid():
        #     dataSerializers.save()
        #     print("here")
        # else:
        #     print("error")

        # print(dataSerializers.data)
        # print("+++++++++++++++++++++++++")
        # print(dataSerializers.errors)


        return Response({"message":"Test","success":True},status=status.HTTP_200_OK)



        


