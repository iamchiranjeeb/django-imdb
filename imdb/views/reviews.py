from rest_framework.response import Response
from rest_framework import status,viewsets
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from imdb.serializers import ReviewSerializer,MovieSerializer,ReviewUpdateSerializers
from imdb.models import Reviews,MoviesModel
from rest_framework.exceptions import ValidationError
from rest_framework import generics
        
class ReviewsView(generics.ListCreateAPIView):
    
    queryset = Reviews.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]
    
    def list(self,request):

        queryset = self.get_queryset()
        serializers = ReviewSerializer(queryset,many=True)
        return Response({"message":serializers.data,"success":True},status=status.HTTP_200_OK)
    
    def perform_create(self, serializer):
        
        pk = self.kwargs.get('pk')
        movie = MoviesModel.objects.get(pk=pk)
        review_user = self.request.user

        review_query_set = Reviews.objects.filter(movies=movie,reviewed_user=review_user)

        if review_query_set.exists():
            raise ValidationError("review done already.")

        if movie.avg_rating == 0:
             movie.avg_rating = serializer.validated_data['rating']
        else:
            movie.avg_rating = (movie.avg_rating+serializer.validated_data['rating'])/2

        movie.total_ratings += 1
        movie.save()
        serializer.save(movies=movie,reviewed_user=review_user)



class ReviewDetails(generics.RetrieveUpdateDestroyAPIView):

    queryset = Reviews.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]
    lookup_fields = ['rating', 'description']

    def get(self, request, *args, **kwargs):

        try:

            reviewPk = kwargs.get('pk')

            review_data = Reviews.objects.get(pk=reviewPk)
            serializer = self.get_serializer(review_data)

            return Response({"message":serializer.data,"success":True},status=status.HTTP_200_OK)
        
        except Reviews.DoesNotExist:
            return Response({"message":f"Review Does Not Exist With Id {reviewPk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        


    def update(self, request, *args, **kwargs):
    
        review_user = request.user
        reviewPk = kwargs.get('pk')

        review_query_set = Reviews.objects.get(pk=reviewPk)
        serializer = self.get_serializer(review_query_set,data=request.data,partial=True)

        if serializer.is_valid():

            self.perform_update(serializer)
            movie = MoviesModel.objects.get(pk=review_query_set.movies.id)
            movies = MovieSerializer(movie)
            totalRating=0

            for singleReview in movies.data['reviews']:
                totalRating += singleReview['rating']

            movie.avg_rating = totalRating/movies.data['total_ratings']
            movie.save()
            return Response({"data":serializer.data,"success":True},status=status.HTTP_201_CREATED)
        
        else:

            return Response({"message":serializer.errors,"success":False},status=status.HTTP_400_BAD_REQUEST)
        

    def delete(self, request, *args, **kwargs):
        
        try:

            reviewPk = kwargs.get('pk')

            instance = self.get_object()
            movieId = instance.movies.id

            deleteResult = self.perform_destroy(instance)

            movie = MoviesModel.objects.get(pk=movieId)
            movies = MovieSerializer(movie)
            totalRating=0


            for singleReview in movies.data['reviews']:
                totalRating += singleReview['rating']

            totalNumberReviews = movies.data['total_ratings']-1
            movie.avg_rating = totalRating/totalNumberReviews
            movie.total_ratings = totalNumberReviews
            movie.save()


            return Response({"message":f"{reviewPk} deleted","success":True},status=status.HTTP_200_OK)
        
        except Reviews.DoesNotExist:
            return Response({"message":f"Review Does Not Exist With Id {reviewPk}","success":False},status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            print(e)
            return Response({"message":e.__class__.__name__,"success":False},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
