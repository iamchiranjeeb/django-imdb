from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator,MaxValueValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .users import User
from .movies import MoviesModel

class Reviews(models.Model):

    class Meta:
        db_table = 'movie_reviews'

    reviewed_user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(validators=[MinValueValidator(1),MaxValueValidator(5)],default=0)
    description = models.CharField(max_length=255,null=True)
    movies = models.ForeignKey(MoviesModel,on_delete=models.CASCADE,related_name="reviews")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.rating) + " " + self.movies.name