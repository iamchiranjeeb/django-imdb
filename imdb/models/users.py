from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator,MaxValueValidator
from django.db.models.signals import post_save,pre_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from s3direct.fields import S3DirectField


class User(AbstractUser):

    class Meta:
        db_table = 'users'

    email = models.EmailField(unique=True)
    username = models.CharField(max_length=40, unique=True)
    phone_no = models.CharField(max_length = 10)
    bio = models.CharField(max_length =500)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
    
    def __str__(self):
        return self.username


class ProfilePhotoModel(models.Model):

    class Meta:
        db_table = 'profile_picture'

    # photo = models.ImageField()
    photo_name = models.CharField(max_length = 100)
    user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)

    def __str__(self):
        return self.photo_name

class ProfilePhoto(models.Model):

    picture = models.ImageField(upload_to = 'media/')
        

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        p=Token.objects.create(user=instance)
        # print("P---->")
        # print(p)


@receiver(pre_delete, sender=settings.AUTH_USER_MODEL)
def delete_related_platform_movie_reviews(sender, instance, **kwargs):
    from .reviews import Reviews
    Reviews.objects.filter(id=instance.id).delete()