from django.db import models
from django.core.validators import MinValueValidator,MaxValueValidator


class StreamPlatform(models.Model):

    class Meta:
        db_table = 'stream_platform'

    name = models.CharField(max_length=30)
    about = models.CharField(max_length=150)
    website = models.URLField(max_length=150)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name