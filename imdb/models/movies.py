from django.db import models
from .platform import StreamPlatform
from django.core.validators import MinValueValidator,MaxValueValidator


class MoviesModel(models.Model):

    class Meta:
        db_table = 'movies'

    name = models.CharField(max_length=50)
    platform = models.ForeignKey(StreamPlatform,on_delete=models.CASCADE,related_name="movies")
    avg_rating = models.FloatField(default=0)
    total_ratings = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name