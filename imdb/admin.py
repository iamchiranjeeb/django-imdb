from django.contrib import admin
from imdb.models import User,StreamPlatform,MoviesModel,ProfilePhoto,ProfilePhotoModel,Reviews
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(User,UserAdmin)
admin.site.register(StreamPlatform)
admin.site.register(MoviesModel)
admin.site.register(ProfilePhoto)
admin.site.register(ProfilePhotoModel)
admin.site.register(Reviews)