from django.conf import settings
from django.core.management.base import BaseCommand
from imdb.models.users import User
from django.conf import settings


class Command(BaseCommand):

    def handle(self,*args, **options):
        
        for user in settings.ADMINS:
            
            username = user[0]
            email= user[1]
            password = 'testadmin'
            print('Creating Account for {},{}'.format(username,email))
            admin = User.objects.create_superuser(email=email, username=username, password=password)
            admin.is_active = True
            admin.is_admin = True
            admin.save()