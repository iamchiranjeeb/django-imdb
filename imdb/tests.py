from rest_framework.test import APITestCase, APIClient
from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from imdb.models import User
import json


class UserCreateTestCase(APITestCase):

    def test_create_user(self):

        data = {
            "username":"chiranjeeb3",
            "first_name":"Chandan",
            "last_name":"Chiranjeeb",
            "email":"chiru3@io.com",
            "phone_no":"9348768387",
            "bio":"m the looser",
            "password":"chiranjeeb",
            "password2":"chiranjeeb"
        }

        response = self.client.post(reverse('register-users'),data)
        self.assertEqual(response.status_code,status.HTTP_201_CREATED)


class LoginLogoutTest(APITestCase):

    data = {
        "username":"chiranjeeb3",
        "first_name":"Chandan",
        "last_name":"Chiranjeeb",
        "email":"chiru3@io.com",
        "phone_no":"9348768387",
        "bio":"m the looser",
        "password":"chiranjeeb",
        "password2":"chiranjeeb"
    }

    login_payload = {
        "email":data['email'],
        "password":data['password']
    }

    accessToken =  None


    def setUp(self):
        
        self.user = User.objects.create_user(
            username=self.data['username'],first_name=self.data['first_name'],last_name=self.data['last_name'],
            email=self.data['email'],phone_no=self.data['phone_no'],bio=self.data['bio'],password=self.data['password']
        )
    
    def test_jwt_auth(self):

        response = self.client.post(reverse('token_obtain_pair'),json.dumps(self.login_payload),content_type="application/json")
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.accessToken = response.json()['access']
        self.refreshToken = response.json()['refresh']

    def test_logout_with_jwt(self):

        response = self.client.post(reverse('token_obtain_pair'),json.dumps(self.login_payload),content_type="application/json")
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.accessToken = response.json()['access']
        self.refreshToken = response.json()['refresh']

        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + self.accessToken)
        response = self.client.post(reverse('logout-users'))
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        self.assertEqual(response.json()['success'],True)

        response = self.client.post(reverse('logout-users'))
        print(response.json())
