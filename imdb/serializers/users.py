from rest_framework import serializers
from imdb.models import User
from imdb.models import ProfilePhotoModel

class UserSerializers(serializers.ModelSerializer):

    password2 = serializers.CharField(style={'input_type':'password'},write_only=True)

    class Meta:
        
        model = User
        fields = ["username","first_name","last_name","email","password","phone_no","bio","password2"]

    def save(self):

        password = self.validated_data['password']
        password2 = self.validated_data['password2']
        userEmail = self.validated_data['email']
        userName = self.validated_data['username']
        firstName = self.validated_data['first_name']
        lastName = self.validated_data['last_name']
        phone_no = self.validated_data['phone_no']
        bio = self.validated_data['bio']

        if password != password2:
            raise serializers.ValidationError({"error":"P1 && P2 should be same !"})

        if User.objects.filter(email=userEmail).exists():
            raise serializers.ValidationError({"error":f"{userEmail} is already in use."})

        account = User(email=userEmail,username=userName,first_name=firstName,last_name=lastName,phone_no=phone_no,bio=bio)
        account.set_password(password)
        account.save()

        return account
        

class UserViewSerializers(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ["username","id"]

class ProfilePhotoSerializer(serializers.Serializer):

    user = UserSerializers(read_only=True)

    class Meta:
        model = ProfilePhotoModel
        fields = "__all__"
