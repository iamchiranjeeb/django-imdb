from rest_framework import serializers
from imdb.models import MoviesModel
from .reviews import ReviewSerializer


class MovieSerializer(serializers.ModelSerializer):

    reviews = ReviewSerializer(many=True,read_only=True)

    class Meta:
        model = MoviesModel
        fields = "__all__"