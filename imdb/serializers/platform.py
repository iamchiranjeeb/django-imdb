from rest_framework import serializers
from imdb.models import StreamPlatform
from .movies import MovieSerializer


class StreamPlatformSerializer(serializers.ModelSerializer):

    movies = MovieSerializer(many=True,read_only=True)

    class Meta:
        model = StreamPlatform
        fields = "__all__"
