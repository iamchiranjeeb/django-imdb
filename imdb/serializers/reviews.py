from rest_framework import serializers
from imdb.models import Reviews
from .users import UserViewSerializers


class ReviewSerializer(serializers.ModelSerializer):

    # len_name = serializers.SerializerMethodField()
    # reviewed_user = serializers.StringRelatedField(read_only=True)
    # reviewed_user = UserViewSerializers(read_only=True)
    reviewed_user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Reviews
        # fields = "__all__"
        exclude = ["movies"]
        # fields = ["movies"]


class ReviewUpdateSerializers(serializers.ModelSerializer):
    # reviewed_user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Reviews
        fields = ["rating","description"]

class ReviewDetailsSerializer(serializers.ModelSerializer):

    reviewed_user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Reviews
        fields = "__all__"