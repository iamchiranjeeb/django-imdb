from django.contrib import admin
from django.urls import path,include
from rest_framework.authtoken.views import obtain_auth_token
from .views import UsersView,UserLogoutView,PlatformView,\
    PlatformDetailsView,MovieView,MovieDetailsView,UploadPhoto,ReviewsView,ReviewDetails
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [

    ##users-api
    path('register/',  UsersView.as_view(), name="register-users"),
    path('logout/',  UserLogoutView.as_view(), name="logout-users"),
    # path('login/',obtain_auth_token,name='login'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('photo/',UploadPhoto.as_view(),name='profile-pic'),

    ##platforms-apis
    path('platform/',  PlatformView.as_view(), name="add-platform"),
    path('platform/<int:pk>/',  PlatformDetailsView.as_view(), name="update-platform"),

    ##movies-api
    path('movie/',  MovieView.as_view(), name="movies"),
    path('movie/<int:pk>/',  MovieDetailsView.as_view(), name="movies-details"),

    ##reviews-api
    path('reviews/',  ReviewsView.as_view(), name="reviews"),
    path('reviews/<int:pk>/create/',  ReviewsView.as_view(), name="reviews-create"),
    path('reviews/<int:pk>/details/',  ReviewDetails.as_view(), name="reviews-deatils")
]