#!/bin/bash

while ! nc -z db 4002 ; do
    echo "Waiting for the MySQL Server"
    sleep 10
done

python manage.py migrate
python manage.py initadmin
exec "$@"