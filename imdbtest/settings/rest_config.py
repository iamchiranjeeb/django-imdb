from datetime import timedelta


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    # 'EXCEPTION_HANDLER': 'rest_framework.views.exception_handler'
}


SIMPLE_JWT = {
    'ROTATE_REFRESH_TOKENS' : True,
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=60),
}