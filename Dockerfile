FROM python:3.8

ENV PYTHONBUFFERED 1

WORKDIR /my_app

COPY . /my_app

RUN chmod +x /my_app/migrate.sh

RUN apt-get update && apt-get install -y netcat

RUN pip install --upgrade pip && pip3 install -r requirements.txt